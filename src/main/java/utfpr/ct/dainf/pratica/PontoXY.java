/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861840
 */
public class PontoXY extends Ponto2D{
    private final double x;
    private final double y;
    public PontoXY(double x, double y)
    {
        super(x, y, 0);
        this.x = x;
        this.y = y;
    }
    @Override
    public String toString()
    {
        return this.getNome()+"("+x+","+y+")";
    }
}
