/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861840
 */
public class PontoXZ extends Ponto2D{
    private final double x;
    private final double z;
    public PontoXZ(double x, double z)
    {
        super(x, 0, z);
        this.x = x;
        this.z = z;
    }
    @Override
    public String toString()
    {
        return this.getNome()+"("+x+","+z+")";
    }
}
