/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.pratica;

/**
 *
 * @author a1861840
 */
public class PontoYZ extends Ponto2D{
    private final double y;
    private final double z;
    public PontoYZ(double y, double z)
    {
        super(0, y, z);
        this.y = y;
        this.z = z;
    }
    @Override
    public String toString()
    {
        return this.getNome()+"("+y+","+z+")";
    }
}

